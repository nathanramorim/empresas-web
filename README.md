# README

# Empresas Web

Listagem de empresas, conforme disponibilizado as instruções.

### Start

---

Para inicializar a aplicação deverá ser executado os comandos em sequência:

```bash
// para os que utilizando npm
npm install

// para os que utilizando yarn
yarn install
```

```bash
// para os que utilizando npm
npm start

// para os que utilizando yarn
yarn start
```

### SignIn

---

![https://i.ibb.co/4P4FWt2/Captura-de-Tela-2020-08-24-a-s-07-29-22.png](https://i.ibb.co/4P4FWt2/Captura-de-Tela-2020-08-24-a-s-07-29-22.png)

https://i.ibb.co/4P4FWt2/Captura-de-Tela-2020-08-24-a-s-07-29-22.png

Para efetuar login eu validei apenas para o usuário

**E-mail**: ioasys@ioasys.com

**Senha**: ioasys

### Dificuldades

---

Tive dificuldades de acesso à API disponibilizada, a mesma encontra-se instável e nem sempre (quase nunca) as requisições obtém-se respostas.

Dados as dificuldades eu gerei um **mock estático** mesmo e fiz a estrutura de service baseado no arquivo `data.ts`

```tsx
const data: EmpresaProps[] = [
    {
        id: 1,
        image: 'https://cdn.pixabay.com/photo/2018/03/31/11/49/company-3277947_960_720.jpg',
        title: 'Empresa 1',
        subtitle: 'Negócio',
        country: 'Brasil',
        description:'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
  },
];
```

![https://i.ibb.co/w6MQYbP/Captura-de-Tela-2020-08-24-a-s-07-24-14.png](https://i.ibb.co/w6MQYbP/Captura-de-Tela-2020-08-24-a-s-07-24-14.png)

https://i.ibb.co/w6MQYbP/Captura-de-Tela-2020-08-24-a-s-07-24-14.png

![https://i.ibb.co/dkrn07y/Captura-de-Tela-2020-08-23-a-s-15-17-44.png](https://i.ibb.co/dkrn07y/Captura-de-Tela-2020-08-23-a-s-15-17-44.png)

https://i.ibb.co/dkrn07y/Captura-de-Tela-2020-08-23-a-s-15-17-44.png