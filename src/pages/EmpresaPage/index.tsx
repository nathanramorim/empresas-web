import React from 'react';
import { useHistory, RouteComponentProps } from 'react-router-dom';
import Main from '../../templates/Main';
import Topbar from '../../components/Topbar';

import './styles.scss';

import { IconButton } from '@material-ui/core';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import EmpresaService from '../../services/EmpresaService';
import EmpresaProps from '../../model/Empresa';

type Params  = {
	id: string;
}

const EmpresaPage = ({match}: RouteComponentProps<Params>) => {
	const empresaService = new EmpresaService();
	const history = useHistory();
	const [empresa, setEmpresa] = React.useState<EmpresaProps>();
	React.useEffect(() => {
		let fetchEmpresa = async () => {
			let result =  await empresaService.find(Number(match.params.id))
			setEmpresa(result);
		}
		fetchEmpresa();
		return () => {
			setEmpresa(undefined);
		}
		// eslint-disable-next-line
	}, [])

	const handleBack = () => {
		history.goBack();
	}
	
	return (
		<Main>
			<Topbar>
				<section className="header-navigation">
						<IconButton className="back-icon" onClick={handleBack}>
							<ArrowBackIcon color="inherit"/>
						</IconButton>
						<h1>{empresa?.title}</h1>
				</section>
			</Topbar>
			<section className="container container-empresa">
				<article className="card-empresa">
						<header className="card-header">
									<span>{empresa?.title}</span>
						</header>
						<p>
							{empresa?.description}
						</p>
				</article>				
			</section>
		</Main>
	);
};

export default EmpresaPage;
