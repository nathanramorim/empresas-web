import React, { FormEvent } from 'react';
import {
	Container,
	FormHelperText,
	InputAdornment,
	IconButton,
	TextField,
} from '@material-ui/core';
import { useHistory } from 'react-router-dom';

import { Visibility, VisibilityOff } from '@material-ui/icons/';
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import LockOpenIcon from '@material-ui/icons/LockOpen';
import ErrorIcon from '@material-ui/icons/Error';
import logoHome from '../../assets/images/logo-home.svg';

import './styles.scss';

import LoadingPage from '../../components/LoadingPage';
import isBlank from '../../utils/isBlank';
import Main from '../../templates/Main';
import AuthService from '../../services/AuthService';
import Credentials from '../../model/Credentials';

const errors = {
	credentials: {
		invalid: 'Credenciais informadas são inválidas, tente novamente.',
	},
	email: {
		presence: 'É necessário o preenchimento do campo de e-mail.',
	},
	password: {
		presence: 'É necessário o preenchimento do campo de senha.',
	},
};

const initialsControls = {
	showPassword: false,
	hasError: {
		credentials: false,
		email: false,
		password: false,
	},
	loading: false,
};

const initalsFormStates = {
	formIsValid: true,
	values: {
		email: '',
		password: '',
	},
	touched: {
		email: false,
		password: false,
	},
};

interface CredentialsProps {
	email: string;
	password: string;
}

const stringEmpty = '';

const LoginPage: React.FC = () => {
	const history = useHistory();
	const [form, setForm] = React.useState(initalsFormStates);
	const [controls, setControls] = React.useState(initialsControls);
	const authService = new AuthService();

	React.useEffect(() => {
		validate(form.values);
	}, [form.values]);

	React.useEffect(() => {
		setForm((form) => ({
			...form,
			formIsValid:
				controls.hasError.email ||
				controls.hasError.password ||
				controls.hasError.credentials,
		}));
	}, [controls]);

	const validate = (values: CredentialsProps) => {
		setControls((controls) => ({
			...controls,
			hasError: {
				...controls.hasError,
				email: isBlank(values.email),
				password: isBlank(values.password),
			},
		}));
	};

	const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		e.persist();
		setForm((form) => ({
			...form,
			values: {
				...form.values,
				[e.target.name]: e.target.value,
			},
			touched: {
				...form.touched,
				[e.target.name]: true,
			}
		}));
		setControls(initialsControls)
	};

	const handleSubmit = async (e: FormEvent) => {
		e.preventDefault();
		setControls({ ...controls, loading: true });
		let { password, email } = form.values;
		let credentials = { password, email } as Credentials
		let result = await authService.signIn(credentials)
		if(result){
			setTimeout(() => {
				history.push('/home');
			}, 2000);
		}else{
			setControls(controls => ({
				...controls, 
				loading: false, 
				hasError: {
					...controls.hasError, 
					credentials: true
				} 
			}));
		}
	};

	const handleClickShowPassword = () => {
		setControls({ ...controls, showPassword: !controls.showPassword });
	};

	const showPasswordButton = (
		<IconButton
			aria-label="toggle password visibility"
			onClick={handleClickShowPassword}
		>
			{controls.showPassword ? <Visibility /> : <VisibilityOff />}
		</IconButton>
	);

	const hasError = (field: string) => {
		switch (field) {
			case 'email':
				return controls.hasError.email && form.touched.email;
			case 'password':
				return controls.hasError.password && form.touched.password;
			default:
				return true;
		}
	};

	const errorIcon = <ErrorIcon color="error" />;

	return (
		<Main vAlign="center">
			<section className="container container-login">
				<Container maxWidth="sm">
					<LoadingPage openProps={controls.loading} />
					<header>
						<img src={logoHome} alt="Logo ioasys" />
						<section>
							<p>BEM-VINDO AO EMPRESAS</p>
							<p className="information">
								Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc
								accumsan.
							</p>
						</section>
					</header>
					<form onSubmit={handleSubmit}>
						<main>
							<TextField
								error={hasError('email')}
								id="email"
								name="email"
								label="E-mail"
								type="email"
								color="secondary"
								className="input-text"
								fullWidth
								value={form.values.email}
								onChange={handleChange}
								InputProps={{
									startAdornment: (
										<InputAdornment position="start">
											<MailOutlineIcon color="secondary" />
										</InputAdornment>
									),
									endAdornment: (
										<InputAdornment position="end">
											{hasError('email') || controls.hasError.credentials
												? errorIcon
												: stringEmpty}
										</InputAdornment>
									),
								}}
								helperText={hasError('email') && errors.email.presence}
							/>

							<TextField
								error={hasError('password')}
								id="password"
								name="password"
								type={controls.showPassword ? 'text' : 'password'}
								label="Senha"
								color="secondary"
								className="input-text"
								fullWidth
								value={form.values.password}
								onChange={handleChange}
								InputProps={{
									startAdornment: (
										<InputAdornment position="start">
											<LockOpenIcon color="secondary" />
										</InputAdornment>
									),
									endAdornment: (
										<InputAdornment position="end">
											{hasError('password') || controls.hasError.credentials
												? errorIcon
												: showPasswordButton}
										</InputAdornment>
									),
								}}
								helperText={hasError('password') && errors.password.presence}
							/>
							{controls.hasError.credentials && (
								<FormHelperText error className="error-text">
									{errors.credentials.invalid}
								</FormHelperText>
							)}
						</main>
						<footer>
							<button type="submit" disabled={form.formIsValid}>
								Entrar
							</button>
						</footer>
					</form>
				</Container>
			</section>
		</Main>
	);
};

export default LoginPage;
