const { v4: uuidv4 } = require('uuid');
const data = [
	{
		id: uuidv4(),
		title: 'Empresa 1',
		subtitle: 'Negócio',
		country: 'Brasil',
	},
	{
		id: uuidv4(),
		title: 'Empresa 2',
		subtitle: 'Negócio',
		country: 'Brasil',
	},
	{
		id: uuidv4(),
		title: 'Empresa 3',
		subtitle: 'Negócio',
		country: 'Brasil',
	},
	{
		id: uuidv4(),
		title: 'Empresa 4',
		subtitle: 'Negócio',
		country: 'Brasil',
	},
	{
		id: uuidv4(),
		title: 'Empresa 5',
		subtitle: 'Negócio',
		country: 'Brasil',
	},
];

export default data;
