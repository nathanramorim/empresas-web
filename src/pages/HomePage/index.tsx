import React from 'react';
import { ThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import {
	TextField,
	InputAdornment,
	IconButton,
	Zoom,
} from '@material-ui/core';

import SearchIcon from '@material-ui/icons/Search';
import CloseIcon from '@material-ui/icons/Close';
import imgEmpresa from '../../assets/images/img-e-1-lista.png';
import ItemEmpresa from '../../components/ItemEmpresa';
import LogoNav from '../../assets/images/logo-nav.svg';
import Main from '../../templates/Main';

import './styles.scss';
import Topbar from '../../components/Topbar';
import EmpresaService from '../../services/EmpresaService';
import EmpresaProps from '../../model/Empresa';

const theme = createMuiTheme({
	palette: {
		primary: {
			main: '#fff',
		},
	},
});

const HomePage: React.FC = () => {
	const [showSearch, setShowSearch] = React.useState(false);
	const [searchValue, setSearchValue] = React.useState('');
	const [empresas, setEmpresas] = React.useState<EmpresaProps[]>([])
	const [empresasFiltered, setEmpresasFiltered] = React.useState<EmpresaProps[]>([]);
	const empresaService = new EmpresaService();
	
	React.useEffect(() => {
		let fetchEmpresas = async () => {
			let result = await empresaService.index();
			setEmpresas(result);
			setEmpresasFiltered(result);
		}
		fetchEmpresas();
		return () => {
			setEmpresas([])
		}
		// eslint-disable-next-line
	},[]);

	React.useEffect(() => {
		filterEmpresa(searchValue);
		return () => {
			setEmpresasFiltered([]);
		};
		// eslint-disable-next-line
	}, [searchValue]);


	const filterEmpresa = (search: string) => {
		let result = empresas.filter((empresa) => {
			return empresa.title.toLowerCase().indexOf(search.toLowerCase()) !== -1;
		});
		setEmpresasFiltered(result);
	};

	const handleSearch = () => {
		setShowSearch(true);
	};

	const handleClickAway = () => {
		setShowSearch(false);
		setEmpresasFiltered(empresas);
	};

	const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		setSearchValue(e.target.value);
	};

	return (
		<Main>
			<Topbar>
				{!showSearch ? (
					<React.Fragment>
						<Zoom in={!showSearch}>
							<img src={LogoNav} alt="Logo ioasys" />
						</Zoom>
						<IconButton
							className="icon-search"
							aria-label="upload picture"
							component="span"
							onClick={handleSearch}
						>
							<SearchIcon />
						</IconButton>
					</React.Fragment>
				) : (
					<ThemeProvider theme={theme}>
							<Zoom in={showSearch}>
								<TextField
									color="primary"
									fullWidth
									id="standard-start-adornment"
									className="input-search"
									value={searchValue}
									onChange={handleChange}
									InputProps={{
										startAdornment: (
											<InputAdornment position="start">
												<SearchIcon color="primary"/>
											</InputAdornment>
										),
										endAdornment: (
											<InputAdornment position="end">
												<IconButton onClick={handleClickAway}>
													<CloseIcon className="icon-search" />
												</IconButton>
											</InputAdornment>
										),
									}}
								/>
							</Zoom>
					</ThemeProvider>
				)}
			</Topbar>
			<section className="container container-home">
				<p>Clique na busca para iniciar.</p>
				{empresasFiltered.map((empresa) => {
					return (
						<ItemEmpresa
							key={empresa.id}
							item={{...empresa, image:imgEmpresa}}
						/>
					);
				})}
			</section>
		</Main>
	);
};

export default HomePage;
