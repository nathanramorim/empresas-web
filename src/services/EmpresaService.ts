import EmpresaProps from '../model/Empresa';
import data from './data';

export default class EmpresaService {
  index (): EmpresaProps[]{
    return data;
  }
  find (id:number): EmpresaProps {
    return data.filter(empresa => {
      return empresa.id === id;
    })[0]
  }
}