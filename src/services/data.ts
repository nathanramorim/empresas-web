import EmpresaProps from '../model/Empresa';

const data: EmpresaProps[] = [
	{
		id: 1,
		image: 'https://cdn.pixabay.com/photo/2018/03/31/11/49/company-3277947_960_720.jpg',
		title: 'Empresa 1',
		subtitle: 'Negócio',
		country: 'Brasil',
		description:'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
	},
	{
		id: 2,
		image: 'https://cdn.pixabay.com/photo/2018/03/31/11/49/company-3277947_960_720.jpg',
		title: 'Empresa 2',
		subtitle: 'Negócio',
		country: 'Brasil',
		description:'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
	},
	{
		id: 3,
		image: 'https://cdn.pixabay.com/photo/2018/03/31/11/49/company-3277947_960_720.jpg',
		title: 'Empresa 3',
		subtitle: 'Negócio',
		country: 'Brasil',
		description:'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
	},
	{
		id: 4,
		image: 'https://cdn.pixabay.com/photo/2018/03/31/11/49/company-3277947_960_720.jpg',
		title: 'Empresa 4',
		subtitle: 'Negócio',
		country: 'Brasil',
		description:'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
	},
	{
		id: 5,
		image: 'https://cdn.pixabay.com/photo/2018/03/31/11/49/company-3277947_960_720.jpg',
		title: 'Empresa 5',
		subtitle: 'Negócio',
		country: 'Brasil',
		description:'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
	},
];

export default data;
