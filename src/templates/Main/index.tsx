import React from 'react';
import './styles.scss';

interface MainProps {
	vAlign?: string
}

const Main: React.FC<MainProps> = (props) => {
	return (
		<React.Fragment>
			<div id="main-layout" style={{alignItems: props?.vAlign}} >{props.children}</div>
		</React.Fragment>
	);
};

export default Main;
