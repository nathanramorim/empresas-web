export default interface EmpresaProps {
  id: number;
	image: string;
	title: string;
	subtitle: string;
  country: string;
  description?: string;
}