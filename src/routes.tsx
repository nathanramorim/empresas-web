import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import LoginPage from './pages/LoginPage';
import HomePage from './pages/HomePage';
import EmpresaPage from './pages/EmpresaPage';

function Routes() {
	return (
		<BrowserRouter>
			<Route path="/" exact component={LoginPage} />
			<Route path="/home" exact component={HomePage} />
			<Route path="/empresa/:id" exact component={EmpresaPage} />
		</BrowserRouter>
	);
}

export default Routes;
