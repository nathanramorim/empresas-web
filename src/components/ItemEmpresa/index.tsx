import React from 'react';
import { Link } from 'react-router-dom';

import './styles.scss';
import EmpresaProps from '../../model/Empresa';
interface ItemEmpresa {
	item: EmpresaProps
}



const ItemEmpresa: React.FC<ItemEmpresa> = (props) => {
	const { item } = props;
	return (
		<React.Fragment>
			<Link to={`/empresa/${item.id}`} className="empresa-item">
				<article className="empresa-item">
						<img src={item.image} alt="Empresa 1" />
						<section>
							<h3 className="title">{item.title}</h3>
							<span className="subtitle">{item.subtitle}</span>
							<span className="country">{item.country}</span>
						</section>
				</article>
			</Link>
		</React.Fragment>
	);
};

export default ItemEmpresa;
