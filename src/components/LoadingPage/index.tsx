import React from 'react';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
	backdrop: {
		zIndex: theme.zIndex.drawer + 1,
		color: '#fff',
	},
}));
interface LoadingPageProps {
	openProps: boolean;
}

const LoadingPage: React.FC<LoadingPageProps> = (props) => {
	const { openProps } = props;
	const [open, setOpen] = React.useState(false);
	const classes = useStyles();

	React.useEffect(() => {
		setOpen(openProps);
	}, [openProps]);

	return (
		<div>
			<Backdrop className={classes.backdrop} open={open}>
				<CircularProgress color="inherit" size={100} />
			</Backdrop>
		</div>
	);
};

export default LoadingPage;
