import React from 'react';

import './styles.scss';

const Topbar = (props: { children: React.ReactNode; }) => {
	return (
			<header id="top-bar">
        <section className="content">
				{props.children}
        </section>
			</header>
	);
};

export default Topbar;
